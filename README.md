#extweb4j
### 介绍

```
1. extweb4j是用ext6+jfinal2开发的轻量级通用WEB基础项目。
2. 包含用户,角色,权限,日志，报表等常用模块，为开发者节省约时间。
3. 使用Ext的最新版本Ext6.0.1，使得开发者不再被UI所困扰,其组件丰富,设计美观,控制精细,多年来深得用户喜爱。
```
### 快速开始

```
1. 打开你的MySql,导入src/main/resources/sql目录下的extweb4j.sql数据库脚本。
2. 将项目导入到eclipse使用maven进行package,然后run即可启动。
3. 打开浏览器输入访问地址（注意上下文需要设置为"/",否则可能出现404异常）,账号/密码:admin/123456。
```
### 截图
![输入图片说明](http://git.oschina.net/uploads/images/2016/0701/180458_8d9d4c1e_89451.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0708/190532_32310826_89451.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0701/180833_af7b0cac_89451.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0708/190605_cd5aebf1_89451.png "在这里输入图片标题")