package com.extweb4j.web.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.extweb4j.core.bean.Msg;
import com.extweb4j.core.comstant.CacheConstant;
import com.extweb4j.core.controller.ExtController;
import com.extweb4j.core.daoaload.MenuAuthDataLoader;
import com.extweb4j.core.kit.ExtKit;
import com.extweb4j.core.kit.LoginKit;
import com.extweb4j.core.model.ExtMenu;
import com.extweb4j.core.model.ExtUser;
import com.extweb4j.web.bean.MenuBean;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.jfinal.plugin.ehcache.CacheKit;
/**
 * 菜单控制器
 * @author Administrator
 *
 */
public class MenuController extends ExtController{
	/**
	 * 获取菜单树,用户根据用户权限加载菜单
	 */
	public void menutree(){
		
		ExtUser user = LoginKit.getSessionUser(getRequest());
		List<ExtMenu> list = CacheKit.get(CacheConstant.GLOBAL_CACHE_NAME,user.getId(), new MenuAuthDataLoader(user.getId()));
		//转换
		List<MenuBean> menuBeans = MenuBean.covertExtMenu(list);
		renderJson(menuBeans);
		
	}
	/**
	 * 是否存在此viewType
	 */
	public void hasViewType(){
		renderJson(new Msg(ExtMenu.dao.hasViewType(getPara("view"))));
	}
	
	/**
	 * 获取菜单树,用户菜单管理
	 */
	public void list(){
		
		String pid  = getPara("node","0");
		
		if(pid.equals("root")){
			pid = "0";
		}
		List<ExtMenu> menuTrees = ExtMenu.dao.getMenuTreeByPid(pid);
		for(ExtMenu menu : menuTrees){
			menu.put("expanded",false);
			menu.set("leaf",menu.getStr("leaf").equals("1")? true : false);
		}
		renderJson(menuTrees);
	}
	/**
	 * 执行新增
	 */
	@Before(Tx.class)
	public void add(){
		
		ExtMenu extMenu = getExtModel(ExtMenu.class);
		ExtMenu menux = ExtMenu.dao.findById(extMenu.getStr("pid"));
		//设置深度
		if(menux!=null){
			String deep = menux.getStr("deep");
			extMenu.set("deep",String.valueOf((Integer.parseInt(deep)+1)));
		}else{
			extMenu.set("deep","1");
			extMenu.set("pid", "0");
		}
		extMenu.set("id", ExtKit.UUID());
		extMenu.set("create_time",new Timestamp(new Date().getTime()));
		extMenu.set("leaf","1");
		extMenu.save();
		//更新父菜单
		if(menux!=null){
			menux.set("leaf", "0");
			menux.update();
		}
		success();
	}
	/**
	 * 编辑
	 */
	@Before(Tx.class)
	public void edit(){
		ExtMenu extMenu = getExtModel(ExtMenu.class);
		
		if(StringUtils.isBlank(extMenu.getStr("pid"))){
			extMenu.set("pid", "0");
		}
		extMenu.update();
		
		ExtMenu menux = ExtMenu.dao.findById(extMenu.getStr("pid"));
		//更新父菜单
		if(menux!=null){
			menux.set("leaf", "0");
			menux.update();
		}
		success();
	}
	/**
	 * 删除
	 */
	@Before(Tx.class)
	public void delete(){
		String id = getPara("id");
		ExtMenu menu = ExtMenu.dao.findById(id);
		String pid = menu.getStr("pid");
		
		ExtMenu.dao.deleteById(id);
		ExtMenu.dao.deleteByAttr("pid",id);
		//更新父菜单
		List<ExtMenu> menus = ExtMenu.dao.findByAttr("pid", pid);
		if(menus.size() == 0){
			ExtMenu.dao.findById(pid).set("leaf", "1").update();
		}
		
		success();
	}
	/**
	 * json
	 */
	public void json(){
		
		String pid  = getPara("node","0");
		
		if(pid.equals("root")){
			pid = "0";
		}
		
		List<ExtMenu> menuTree = ExtMenu.dao.getMenuTreeByPid(pid);
		
		for(ExtMenu menu : menuTree){
			menu.put("expanded",false);
			menu.set("leaf",menu.getStr("leaf").equals("1")? true : false);
		}
		renderJson(menuTree);
	}
}
